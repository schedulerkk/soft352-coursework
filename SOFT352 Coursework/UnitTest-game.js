
// Tests for structure/html
QUnit.test("#Unit test 1 testing page title", function(assert)
{
  assert.ok($(document).attr("title").indexOf("Draw & Guess")!=-1,"Checking the page title");
});

QUnit.test("#Unit test 2 testing content head", function(assert)
{
  assert.equal($(".contentHead")[0].tagName, "DIV", "Checking that a \'contentHead\' div element has been added");
});

QUnit.test("#Unit test 3 testing horizontal content div element presence", function(assert)
{
  assert.equal($(".content-horizontal")[0].tagName, "DIV", "Checking that a \'content-horizontal\' div element has been added");
});

QUnit.test("#Unit test 4 testing player content elements", function(assert)
{
  //console.log("Number of .content-players in .content-horizontal = " + $(".content-horizontal .content-players").length)
  assert.equal($(".content-horizontal .content-players")[0].tagName, "DIV", "Checking that a \'content-players\' div element has been added as a child of \'content-horizontal\'");
  assert.equal($(".content-horizontal .content-players #playerListTitle")[0].tagName, "DIV", "Checking that a \'playerListTitle\' div element has been added as a child of \'content-players\'");
  assert.equal($(".content-horizontal .content-players .player").length, 10, "Checking that all player elements have been added as a child of \'content-players\'");
});

QUnit.test("#Unit test 5 testing vertical content elements", function(assert)
{
  assert.equal($(".content-horizontal .content-vertical")[0].tagName, "DIV", "Checking that a \'content-vertical\' div element has been added as a child of \'content-horizontal\'");

  assert.equal($(".content-horizontal .content-vertical .content-top")[0].tagName, "DIV", "Checking that a \'content-top\' div element has been added as a child of \'content-vertical\'");
  assert.equal($(".content-horizontal .content-vertical .content-top #countingTimer")[0].tagName, "P", "Checking that a \'countingTimer\' p element has been added as a child of \'content-top\'");
  assert.equal($(".content-horizontal .content-vertical .content-top #drawerText")[0].tagName, "P", "Checking that a \'drawerText\' p element has been added as a child of \'content-top\'");
  assert.equal($(".content-horizontal .content-vertical .content-top #clearButton")[0].tagName, "BUTTON", "Checking that a \'clearButton\' button element has been added as a child of \'content-top\'");

  assert.equal($(".content-horizontal .content-vertical .content-drawingCanvas")[0].tagName, "DIV", "Checking that a \'content-drawingCanvas\' div element has been added as a child of \'content-vertical\'");
  assert.equal($(".content-horizontal .content-vertical .content-drawingCanvas #drawingCanvas")[0].tagName, "CANVAS", "Checking that a \'drawingCanvas\' canvas element has been added as a child of \'content-drawingCanvas\'");

  assert.equal($(".content-horizontal .content-vertical .content-input")[0].tagName, "DIV", "Checking that a \'content-input\' div element has been added as a child of \'content-vertical\'");
  assert.equal($(".content-horizontal .content-vertical .content-input #guess")[0].type, "text", "Checking that a \'guess\' text element has been added as a child of \'content-input\'");
  assert.equal($(".content-horizontal .content-vertical .content-input #submitGuess")[0].tagName, "BUTTON", "Checking that a \'submitGuess\' button element has been added as a child of \'content-input\'");
});

QUnit.test("#Unit test 6 testing tool content elements", function(assert)
{
  assert.equal($(".content-horizontal .content-tools")[0].tagName, "DIV", "Checking that a \'content-tools\' div element has been added as a child of \'content-horizontal\'");

  assert.equal($(".content-horizontal .content-tools .tools")[0].tagName, "DIV", "Checking that a \'tools\' div element has been added as a child of \'content-tools\'");
  assert.equal($(".content-horizontal .content-tools .tools #toolText")[0].tagName, "P", "Checking that a \'toolText\' p element has been added as a child of \'tools\'");
  assert.equal($(".content-horizontal .content-tools .tools .toolLists")[0].tagName, "DIV", "Checking that all \'toolLists\' elements have been added as the children of \'tools\'");
  assert.equal($(".content-horizontal .content-tools .tools .list").length, 2, "Checking that all \'list\' elements have been added as the children of \'tools\'");

  assert.equal($(".content-horizontal .content-tools .tools .list #pencil")[0].tagName, "BUTTON", "Checking that a \'pencil\' button element has been added as a child of \'list\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #pencil #pencilImg")[0].type, "image", "Checking that the \'fillImg\' image element has been added as a child of \'pencil\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #eraser")[0].tagName, "BUTTON", "Checking that a \'eraser\' button element has been added as a child of \'list\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #eraser #eraserImg")[0].type, "image", "Checking that the \'fillImg\' image element has been added as a child of \'eraser\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #fill")[0].tagName, "BUTTON", "Checking that a \'fill\' button element has been added as a child of \'list\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #fill #fillImg")[0].type, "image", "Checking that the \'fillImg\' image element has been added as a child of \'fill\'");

  assert.equal($(".content-horizontal .content-tools .tools .list #circle")[0].tagName, "BUTTON", "Checking that a \'circle\' button element has been added as a child of \'list\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #circle #circleImg")[0].type, "image", "Checking that the \'circleImg\' image element has been added as a child of \'circle\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #square")[0].tagName, "BUTTON", "Checking that a \'square\' button element has been added as a child of \'list\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #square #squareImg")[0].type, "image", "Checking that the \'squareImg\' image element has been added as a child of \'square\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #triangle")[0].tagName, "BUTTON", "Checking that a \'triangle\' button element has been added as a child of \'list\'");
  assert.equal($(".content-horizontal .content-tools .tools .list #triangle #triangleImg")[0].type, "image", "Checking that the \'triangleImg\' image element has been added as a child of \'triangle\'");
});

QUnit.test("#Unit test 7 testing color elements", function(assert)
{
  assert.equal($(".content-horizontal .content-tools .color")[0].tagName, "DIV", "Checking that a \'color\' div element has been added as a child of \'content-tools\'");

  assert.equal($(".content-horizontal .content-tools .color #colorText")[0].tagName, "P", "Checking that a \'colorText\' p element has been added as a child of \'color\'");

  assert.equal($(".content-horizontal .content-tools .color .buttons")[0].tagName, "DIV", "Checking that a \'buttons\' div element has been added as a child of \'color\'");
  assert.equal($(".content-horizontal .content-tools .color .buttons .div-color").length, 9, "Checking that all \'div-color\' elements have been added as a child of \'buttons\'");
  assert.equal($(".content-horizontal .content-tools .color .buttons .div-color button").length, 9, "Checking that all button elements have been added as a child of \'div-color\'");
});

QUnit.test("#Unit test 8 testing size elements", function(assert)
{
  assert.equal($(".content-horizontal .content-tools .size")[0].tagName, "DIV", "Checking that a \'size\' div element has been added as a child of \'content-tools\'");
  assert.equal($(".content-horizontal .content-tools .size #sizeText")[0].tagName, "P", "Checking that a \'sizeText\' p element has been added as a child of \'size\'");
  assert.equal($(".content-horizontal .content-tools .size #sizeSlider")[0].type, "range", "Checking that a \'sizeSlider\' range element has been added as a child of \'size\'");
});


// Tests for style/css
// It is hard to test the actual width/height as percentage is used
// Final format may be changed for UI improvement
QUnit.test("#Unit test 9 testing body format", function(assert)
{
  //console.log(parseInt(window.getComputedStyle(document.body).getPropertyValue("margin").replace("px",""), 10));
  assert.equal(parseInt(window.getComputedStyle(document.body).getPropertyValue("margin").replace("px",""), 10), 15, "Checking that the body margin is 15px");
  assert.equal(parseInt(window.getComputedStyle(document.body).getPropertyValue("padding").replace("px",""), 10), 0, "Checking that the body padding is 0px");
});

QUnit.test("#Unit test 10 testing horizontal content", function(assert)
{
  assert.equal($(".content-horizontal").css("display"), "flex", "Checking that the horizontal content has a \'flex\' display property");
});

QUnit.test("#Unit test 11 testing player content", function(assert)
{
  assert.equal($(".content-players").css("display"), "flex", "Checking that the player content has a \'flex\' display property");
  assert.equal($(".content-players").css("flex-direction"), "column", "Checking that the player content has a \'column\' flex-direction property");
  assert.equal($(".content-players").css("padding"), "0px", "Checking that the player content's padding is 0px");

  assert.equal($(".content-players #playerListTitle").css("text-align"), "center", "Checking that the playerListTitle has a \'center\' text-align property");
});

QUnit.test("#Unit test 12 testing player", function(assert)
{
  assert.equal($(".content-players .player").css("border-radius"), "6px", "Checking that the player's border-radius is 6px");
  assert.equal($(".content-players .player").css("text-align"), "center", "Checking that the player has a \'center\' text-align property");
  assert.equal($(".content-players .player").css("color"), "rgb(255, 255, 255)", "Checking that the player's text color is white");
});

QUnit.test("#Unit test 13 testing timer", function(assert)
{
  assert.equal($("#countingTimer").css("text-align"), "center", "Checking that the timer has a \'center\' text-align property");
});

QUnit.test("#Unit test 14 testing input content", function(assert)
{
  assert.equal($(".content-input").css("display"), "flex", "Checking that the input content has a \'flex\' display property");
  assert.equal($(".content-input").css("flex-direction"), "row", "Checking that the input content has a \'row\' flex-direction property");
});

QUnit.test("#Unit test 15 testing tools content", function(assert)
{
  assert.equal($(".content-tools .tools").css("display"), "flex", "Checking that the tools content has a \'flex\' display property");
  assert.equal($(".content-tools .tools").css("flex-direction"), "column", "Checking that the tools content has a \'column\' flex-direction property");

  assert.equal($(".content-tools .tools .toolLists").css("display"), "flex", "Checking that the tool list content has a \'flex\' display property");
  assert.equal($(".content-tools .tools .toolLists").css("flex-direction"), "row", "Checking that the tool list content has a \'row\' flex-direction property");
});

QUnit.test("#Unit test 16 testing tool buttons", function(assert)
{
  assert.equal($(".content-tools .tools .list button").css("border-radius"), "20px", "Checking that the tool buttons' border-radius are 20px");
});

QUnit.test("#Unit test 17 testing tool titles", function(assert)
{
  assert.equal($(".content-tools p").css("text-align"), "center", "Checking that the tool titles has a \'center\' text-align property");
  // Bold = 700
  assert.equal($(".content-tools p").css("font-weight"), "700", "Checking that the tool titles have \'bold\' font-weight properties");
  assert.equal($(".content-tools p").css("margin"), "0px", "Checking that the tool titles margin is 0px");
});

QUnit.test("#Unit test 18 testing color content", function(assert)
{
  assert.equal($(".content-tools .color").css("margin"), "0px", "Checking that the color content's margin is 0px");

  assert.equal($(".content-tools .color .buttons").css("display"), "flex", "Checking that the color button content has a \'flex\' display property");
  assert.equal($(".content-tools .color .buttons").css("flex-direction"), "row", "Checking that the color button content has a \'row\' flex-direction property");

  assert.equal($(".content-tools .color .buttons .div-color").css("text-align"), "center", "Checking that the color buttons are centered");
});

QUnit.test("#Unit test 19 testing size range slider", function(assert)
{
  assert.equal($("#sizeSlider").css("margin"), "0px", "Checking that the size range slider's margin is 0px");
  assert.equal($("#sizeSlider").css("padding"), "0px", "Checking that the size range slider's padding is 0px");
  assert.equal($("#sizeSlider").css("height"), "15px", "Checking that the size range slider's height is 15px");
});
