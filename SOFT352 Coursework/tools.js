
/* Internal variables */
var size = 2;		// Initial size for tools
var pencilSizeMultiply = 3;		// Multiply base of the pencil marks
var shapeSizeMultiply = 6;		// Multiply base of the shape marks
var paintColor = "#000000";		// Initial paint color for tools

var $drawingCanvas;		// Stores the canvas object
var canvasContext;		// Stores the canvas's context

var dragging = false;	// Indicates whether the mouse is dragging
var lastPointX = -1;	// The last point X updated by pencil draw
var lastPointY = -1;	// The last point Y updated by pencil draw
var isDrawingClient;	// Determines whether this client is drawing

/* Fill Color: Code from: https://ben.akrin.com/?p=7888 #4-Algorithm */
function flood_fill( x, y ) {
	// Only the drawing client is sending the drawing data to the server
	if (isDrawingClient)
	{
		sendDrawingToServer(x, y);
	}

	var fillColor = color_to_rgba(paintColor);
	//console.log(fillColor);
	//console.log(drawingCanvas.width);

  pixel_stack = [{x:x, y:y}];
  pixels = canvasContext.getImageData( 0, 0, drawingCanvas.width, drawingCanvas.height ) ;
  var linear_cords = ( y * drawingCanvas.width + x ) * 4 ;
  original_color = {r:pixels.data[linear_cords],
                    g:pixels.data[linear_cords+1],
                    b:pixels.data[linear_cords+2],
                    a:pixels.data[linear_cords+3]} ;

	// Check if the color pixel clicked is the same as fillColor
	// If so, just quit the return process, it is done already!
	if (fillColor.r == original_color.r &&
			fillColor.g == original_color.g &&
			fillColor.b == original_color.b &&
			fillColor.a == original_color.a)
	{
		//console.log("returned");
		return;
	}

  while( pixel_stack.length>0 ) {
      new_pixel = pixel_stack.shift() ;
      x = new_pixel.x ;
      y = new_pixel.y ;

      //console.log( x + ", " + y ) ;

      linear_cords = ( y * drawingCanvas.width + x ) * 4 ;
      while( y-->=0 &&
             (pixels.data[linear_cords]==original_color.r &&
              pixels.data[linear_cords+1]==original_color.g &&
              pixels.data[linear_cords+2]==original_color.b &&
              pixels.data[linear_cords+3]==original_color.a) ) {
          linear_cords -= drawingCanvas.width * 4 ;
      }
      linear_cords += drawingCanvas.width * 4 ;
      y++ ;

      var reached_left = false ;
      var reached_right = false ;
      while( y++<drawingCanvas.height &&
             (pixels.data[linear_cords]==original_color.r &&
              pixels.data[linear_cords+1]==original_color.g &&
              pixels.data[linear_cords+2]==original_color.b &&
              pixels.data[linear_cords+3]==original_color.a) ) {
          pixels.data[linear_cords]   = fillColor.r ;
          pixels.data[linear_cords+1] = fillColor.g ;
          pixels.data[linear_cords+2] = fillColor.b ;
          pixels.data[linear_cords+3] = fillColor.a ;

          if( x>0 ) {
              if( pixels.data[linear_cords-4]==original_color.r &&
                  pixels.data[linear_cords-4+1]==original_color.g &&
                  pixels.data[linear_cords-4+2]==original_color.b &&
                  pixels.data[linear_cords-4+3]==original_color.a ) {
                  if( !reached_left ) {
                      pixel_stack.push( {x:x-1, y:y} ) ;
                      reached_left = true ;
                  }
              } else if( reached_left ) {
                  reached_left = false ;
              }
          }

          if( x<drawingCanvas.width-1 ) {
              if( pixels.data[linear_cords+4]==original_color.r &&
                  pixels.data[linear_cords+4+1]==original_color.g &&
                  pixels.data[linear_cords+4+2]==original_color.b &&
                  pixels.data[linear_cords+4+3]==original_color.a ) {
                  if( !reached_right ) {
                      pixel_stack.push( {x:x+1,y:y} ) ;
                      reached_right = true ;
                  }
              } else if( reached_right ) {
                  reached_right = false ;
              }
          }

          linear_cords += drawingCanvas.width * 4 ;
      }
  }
  canvasContext.putImageData( pixels, 0, 0 ) ;
}

function color_to_rgba( color ) {
  if( color[0]=="#" ) { // hex notation
      color = color.replace( "#", "" ) ;
      var bigint = parseInt(color, 16);
      var r = (bigint >> 16) & 255;
      var g = (bigint >> 8) & 255;
      var b = bigint & 255;
      return {r:r,
              g:g,
              b:b,
              a:255} ;
  } else if( color.indexOf("rgba(")==0 ) { // already in rgba notation
      color = color.replace( "rgba(", "" ).replace( " ", "" ).replace( ")", "" ).split( "," ) ;
      return {r:color[0],
              g:color[1],
              b:color[2],
              a:color[3]*255} ;
  } else if( color.indexOf("rgb(")==0 ) {
      color = color.replace( "rgb(", "" ).replace( " ", "" ).replace( ")", "" ).split( "," ) ;
      return {r:parseInt(color[0].replace( " ", "" )),
              g:parseInt(color[1].replace( " ", "" )),
              b:parseInt(color[2].replace( " ", "" )),
              a:255} ;
  } else {
      console.error( "warning: can't convert color to rgba: " + color ) ;
      return {r:0,
              g:0,
              b:0,
              a:0} ;
  }
}

function setTool(newToolId)
{
	// Deselect the old tool
	var oldTool = $(".tool.selected")[0];
	if (oldTool)
	{
		oldTool.className = "tool";
	}

	// Set the target tool to be selected (for update UI dynamically)
	$("#"+newToolId)[0].className = "tool selected";

	//console.log("Tool changed to " + newToolId);
}

function selectTool(event)
{
	if (!isDrawingClient)
	{
		return;
	}

	// Identify selected tool button
	var tool = event.target;

	// Set the size for the drawer client
	setTool(tool.parentElement.id);

	// Send the server a message to tell the other clients
	// for changing a new color
	sendToolToServer(tool.parentElement.id);
}

function setColor(newColor)
{
	// Deselect the old color
	var oldColor = $(".btn-color.selected")[0];
	if (oldColor)
	{
		oldColor.className = "btn-color";
	}

	// Set the target color to be selected (for update UI dynamically)
	var colorButtons = $(".btn-color");
	for (var i=0; i<colorButtons.length; i++)
	{
		if (getComputedStyle(colorButtons[i], null).getPropertyValue("background-color") === newColor)
		{
			colorButtons[i].className = "btn-color selected";
			break;
		}
	}

	paintColor = newColor;
	//console.log("paintColor changed to " + newColor);
}

function selectColor(event)
{
	// Identify the selected color button
	var colorButton = event.target;

	// Bug fix: https://stackoverflow.com/questions/18608658/cant-get-the-background-color-using-dom
	//console.log(color + "\'s color = " + getComputedStyle(color, null).getPropertyValue("background-color"));
	var color = getComputedStyle(colorButton, null).getPropertyValue("background-color");

	// Set the size for the drawer client
	setColor(color);

	// Send the server a message to tell the other clients
	// for changing a new color
	sendColorToServer(color);
}

function setSize(newSize)
{
	size = newSize;
	//console.log("Size changed to " + newSize);
}

function OnChangedSize(changedSize)
{
	// Set the size for the drawer client
	setSize(changedSize);

	// The drawer sends the server a message to tell the other clients
	// for changing a new size
	sendSizeToServer(changedSize);
}

function drawCanvas(x, y)
{
	// Only the drawing client is sending the drawing data to the server
	if (isDrawingClient)
	{
		sendDrawingToServer(x, y);
	}

	var tool = $(".tool.selected")[0];

	// Check if any tool is selected
	if (tool === undefined)
	{
		return;
	}

	tool = tool.id;

	if (tool == "pencil")
	{
		drawBetweenPoints(x, y, paintColor, size);

		$drawingCanvas.drawArc({
			x: x,
			y: y,
			radius: size * pencilSizeMultiply,
			fillStyle: paintColor
		});

		lastPointX = x;
		lastPointY = y;
	}
	else if (tool == "eraser")
	{
		// Same as pencil, except using white as colorText and bigger size
		/*drawBetweenPoints(x, y, "white", size);

		$drawingCanvas.drawArc({
			x: x,
			y: y,
			radius: size * pencilSizeMultiply * 5,
			fillStyle: "white"
		});

		lastPointX = x;
		lastPointY = y;*/

		// Clear the canvas in a certain size using jcanvas
		$drawingCanvas.clearCanvas({
			x: x,
			y: y,
			width: size * pencilSizeMultiply * 5,
			height: size * pencilSizeMultiply * 5
		});
	}
	else if (tool == "circle")
	{
		$drawingCanvas.drawArc({
			x: x,
			y: y,
			radius: size * 5 * shapeSizeMultiply,
			fillStyle: paintColor
		});

		// Shapes are drawn once only
		if (isDrawingClient && dragging)
		{
			dragging = false;
		}
	}
	else if (tool == "square")
	{
		$drawingCanvas.drawRect({
			x: x,
			y: y,
			width: size * 9 * shapeSizeMultiply,
			height: size * 9 * shapeSizeMultiply,
			fillStyle: paintColor
		});

		// Shapes are drawn once only
		if (isDrawingClient && dragging)
		{
			dragging = false;
		}
	}
	else if (tool == "triangle")
	{
		$drawingCanvas.drawPolygon({
			x: x,
			y: y,
			sides: 3,
			radius: size * 6 * shapeSizeMultiply,
			fillStyle: paintColor
		});

		// Shapes are drawn once only
		if (isDrawingClient && dragging)
		{
			dragging = false;
		}
	}
}

// Create a line between points to smooth out the drawing instead of dots
function drawBetweenPoints(curX, curY)
{
	if (lastPointX != -1 && lastPointY != -1)
	{
		$drawingCanvas.drawLine({
			strokeStyle: paintColor,
			strokeWidth: size * pencilSizeMultiply * 2,	/* Size = radius */
			rounded: true,
			x1: lastPointX, y1: lastPointY,
			x2: curX, y2: curY
		});
	}
}

// Referenced from: https://stackoverflow.com/questions/17130395/real-mouse-position-in-canvas
function GetMousePos(canvas, evt) {
  var rect = canvas.getBoundingClientRect(), // abs. size of element
      scaleX = canvas.width / rect.width,    // relationship bitmap vs. element for X
      scaleY = canvas.height / rect.height;  // relationship bitmap vs. element for Y

  return {
    x: (evt.clientX - rect.left) * scaleX,   // scale mouse coordinates after they have
    y: (evt.clientY - rect.top) * scaleY     // been adjusted to be relative to element
  }
}

var startDragging = function(event)
{
	// Check if this client is supposed to draw on canvas
	if (!isDrawingClient)
	{
		return;
	}

	// Check if any tool is selected
	if ($(".tool.selected")[0] === undefined)
	{
		return;
	}

	dragging = true;

	var pos = GetMousePos(drawingCanvas, event);
	//console.log("PosX = " + pos.x + ", PosY = " + pos.y);

	if ($(".tool.selected")[0].id === "fill")
	{
		dragging = false;
		flood_fill(parseInt(pos.x), parseInt(pos.y));
		//console.log(color_to_rgba(paintColor));
	}
	else
	{
		dragCanvas(event);
	}
}

var dragCanvas = function(event)
{
	// Check if this client is supposed to draw on canvas
	if (!isDrawingClient)
	{
		return;
	}

	if (dragging)
	{
		var pos = GetMousePos(drawingCanvas, event);
		drawCanvas(pos.x, pos.y);
	}
}

function stopDrawing()
{
	dragging = false;
	lastPointX = -1;
	lastPointY = -1;
}

var stopDragging = function()
{
	// Only the drawing client is sending the drawing data to the server
	if (isDrawingClient)
	{
		stopDrawing();

		// Tell the other clients the stopDragging event
		sendStopDrawingToServer();
	}
}

function setDrawingClient(drawing)
{
	// Reset drawing attributes
	stopDrawing();

	isDrawingClient = drawing;

	// Deselect the selected drawing tool (if exist)
	var drawingTool = $(".tool.selected")[0];
	if (drawingTool)
	{
		drawingTool.className = "tool";
	}

	// Deselect the old color
	var oldColor = $(".btn-color.selected")[0];
	if (oldColor)
	{
		oldColor.className = "btn-color";
	}

	// Reset to default drawing tools, colors & values
	$("#pencil")[0].className = "tool selected";
	setColor(getComputedStyle($("#color-black")[0], null).getPropertyValue("background-color"));
	$("#sizeSlider").val(2);
	setSize(2);

	// Change elements' status according to the client's drawing permission
	var status = !drawing;

	// Clear button
	$("#clearButton").prop("disabled", status);

	// Tool buttons
	var tools = $(".tool");
	for (var i=0; i<tools.length; i++)
	{
		tools[i].disabled = status;
	}

	// Color buttons
	var colors = $(".btn-color");
	for(var i=0; i<colors.length; i++)
	{
		colors[i].disabled = status;
	}

	// Size slider
	$("#sizeSlider")[0].disabled = status;
}

/* Use the canvas to display server messages */
function gameStarted()
{
	clearLocalCanvas();

	// Background Color
	$drawingCanvas.drawRect({
		x: 0,
		y: 0,
		width: drawingCanvas.width * 2,
		height: drawingCanvas.height * 2,
		fillStyle: "#3490C9"
	});

	// Rectangular title background
	$drawingCanvas.drawRect({
		strokeStyle: "#A02121",
		strokeWidth: 8,
		x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
		width: 750,
		height: 200,
		cornerRadius: 10
	});

	// Title
	$drawingCanvas.drawText({
    fillStyle: "#E5D7E4",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
    fontSize: "140pt",
    fontFamily: "Ink Free",
    text: ">>  Standby  <<",
    fromCenter: true
  });

	// Content Text
	$drawingCanvas.drawText({
    fillStyle: "#EFB943",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height * 3 / 5,
    fontSize: "80pt",
    fontFamily: "Ink Free",
    text: "The game has started!\nPlease wait for the next round.",
    fromCenter: true
  });
}

function allCorrect()
{
	clearLocalCanvas();

	// Background Color
	$drawingCanvas.drawRect({
		x: 0,
		y: 0,
		width: drawingCanvas.width * 2,
		height: drawingCanvas.height * 2,
		fillStyle: "#3490C9"
	});

	// Rectangular title background
	$drawingCanvas.drawRect({
		strokeStyle: "#A02121",
		strokeWidth: 8,
		x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
		width: 1000,
		height: 200,
		cornerRadius: 10
	});

	// Title
	$drawingCanvas.drawText({
    fillStyle: "#E5D7E4",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
    fontSize: "140pt",
    fontFamily: "Ink Free",
    text: "Well done!",
    fromCenter: true
  });

	// Content Text
	$drawingCanvas.drawText({
    fillStyle: "#EFB943",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height * 3 / 5,
    fontSize: "80pt",
    fontFamily: "Ink Free",
    text: "All guessers got the answer\n\nAll guessers +1 score\nDrawer +2 score",
    fromCenter: true
  });
}

function waitPlayers(playersRequired, gamePaused)
{
	clearLocalCanvas();

	// Background Color
	$drawingCanvas.drawRect({
		x: 0,
		y: 0,
		width: drawingCanvas.width * 2,
		height: drawingCanvas.height * 2,
		fillStyle: "#3490C9"
	});

	// Rectangular title background
	$drawingCanvas.drawRect({
		strokeStyle: "#A02121",
		strokeWidth: 8,
		x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
		width: 1500,
		height: 200,
		cornerRadius: 10
	});

	// Title
	$drawingCanvas.drawText({
    fillStyle: "#E5D7E4",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
    fontSize: "140pt",
    fontFamily: "Ink Free",
    text: gamePaused ? "Game Paused" : "Game Commencing",
    fromCenter: true
  });

	// Content Text
	$drawingCanvas.drawText({
    fillStyle: "#EFB943",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height * 3 / 5,
    fontSize: "80pt",
    fontFamily: "Ink Free",
    text: "Waiting for " + playersRequired + " players to start the game...",
    fromCenter: true
  });
}

function displayAnswer(ans)
{
	clearLocalCanvas();

	// Background Color
	$drawingCanvas.drawRect({
		x: 0,
		y: 0,
		width: drawingCanvas.width * 2,
		height: drawingCanvas.height * 2,
		fillStyle: "#3490C9"
	});

	// Title
	$drawingCanvas.drawText({
    fillStyle: "#E5D7E4",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 4,
    fontSize: "100pt",
    fontFamily: "Ink Free",
    text: "The correct answer is",
    fromCenter: true
  });

	// Answer
	$drawingCanvas.drawText({
    fillStyle: "#EFB943",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height * 3 / 5,
    fontSize: "150pt",
    fontFamily: "Ink Free",
    text: ans,
    fromCenter: true
  });
}

function notifyDrawerLeft()
{
	clearLocalCanvas();

	// Background Color
	$drawingCanvas.drawRect({
		x: 0,
		y: 0,
		width: drawingCanvas.width * 2,
		height: drawingCanvas.height * 2,
		fillStyle: "#3490C9"
	});

	// Title
	$drawingCanvas.drawText({
    fillStyle: "#E5D7E4",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 4,
    fontSize: "180pt",
    fontFamily: "Ink Free",
    text: "orz",
    fromCenter: true
  });

	// Content
	$drawingCanvas.drawText({
    fillStyle: "#EFB943",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height * 3 / 5,
    fontSize: "140pt",
    fontFamily: "Ink Free",
    text: "The drawer has left...",
    fromCenter: true
  });
}

function displayWinners(winners)
{
	clearLocalCanvas();

	// Background Color
	$drawingCanvas.drawRect({
		x: 0,
		y: 0,
		width: drawingCanvas.width * 2,
		height: drawingCanvas.height * 2,
		fillStyle: "#3490C9"
	});

	// Rectangular title background
	$drawingCanvas.drawRect({
		strokeStyle: "#A02121",
		strokeWidth: 8,
		x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
		width: 650,
		height: 200,
		cornerRadius: 10
	});

	// Title
	$drawingCanvas.drawText({
    fillStyle: "#E5D7E4",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 6,
    fontSize: "140pt",
    fontFamily: "Ink Free",
    text: winners.length>1 ? "Winners" : "Winner",
    fromCenter: true
  });

	// Winner(s)
	$drawingCanvas.drawText({
    fillStyle: "#EFB943",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width / 2,
    y: drawingCanvas.height / 2,
    fontSize: "80pt",
    fontFamily: "Ink Free",
    text: winners.toString(),
    fromCenter: true
  });

	// Congrats Text
	$drawingCanvas.drawText({
    fillStyle: "#975AA7",
    strokeStyle: "#333",
    strokeWidth: 2,
    x: drawingCanvas.width * 3 / 4,
    y: drawingCanvas.height * 3 / 4,
    fontSize: "90pt",
    fontFamily: "Ink Free",
    text: "Congratulations!",
    fromCenter: true,
    rotate: 345
  });

	// Congrats underline
	$drawingCanvas.drawLine({
		strokeStyle: "#BBE276",
		strokeWidth: 10,
	  rounded: true,
	  x1: 1084, y1: 911,
	  x2: 1880, y2: 675,
		x3: 1267, y3: 918,
	  x4: 1769, y4: 756
	});
}

// Clears the canvas locally
function clearLocalCanvas()
{
	//canvasContext.clearRect(0, 0, drawingCanvas.width, drawingCanvas.height);
	$drawingCanvas.clearCanvas();
}

function clearCanvas()
{
	clearLocalCanvas();

	// Send the server a message to tell the other clients
	// to clear their canvas too
	sendClearToServer();
}

// --- Main functions calling/Setups ---
$(document).ready(function()
{
	// --- jCanvas ---
	$drawingCanvas = $("#drawingCanvas");
	canvasContext = drawingCanvas.getContext("2d");

	drawingCanvas.width = window.innerWidth;
	drawingCanvas.height = window.innerHeight;

	drawingCanvas.addEventListener("mousedown", startDragging);
	drawingCanvas.addEventListener("mousemove", dragCanvas);
	drawingCanvas.addEventListener("mouseup", stopDragging);

	// Clear canvas click listeners
	$("#clearButton").click(clearCanvas);

	// Tool button click listeners
	var tools = document.getElementsByClassName("tool");
	for(var i=0; i<tools.length; i++)
	{
		tools[i].addEventListener("click", selectTool);
	}

	// Color button click listeners
	var colors = document.getElementsByClassName("btn-color");
	for(var i=0; i<colors.length; i++)
	{
		colors[i].addEventListener("click", selectColor);
	}

	// Bug fix: Stop drawing if the mouse is out of the canvas
	$drawingCanvas.mouseleave(stopDragging);

	// For testing purposes only
	//setDrawingClient(true);
	//gameStarted();
	//allCorrect();
	//waitPlayers(2, true);
	//displayAnswer("cloud");
	//notifyDrawerLeft();
	//displayWinners(["TestPlayer", "TestPlayer", "TestPlayer"]);
});
