
/* Configuration */
var usernameLength_min = 3;   // The minimum length of a username
var usernameLength_max = 10;  // The maximum length of a username

/* Internal variables */
var port = 9000;	  // The server's port number
var connection;     // Stores a client websocket object

var username;       // This client's username
var score;          // The current score of this client from server
var winningScore;   // The winning score from server
var timeRemaining;  // The remaining time in seconds from server
var timeCounting;		// Stores the id for clearInterval

function checkGuessValid(input)
{
  // Regular expression
  var regex = /[^a-z]/gi;
  input.value = input.value.replace(regex, "");
}

// Update Players' scores to UI
function updatePlayerList(players, scores, playerStatuses)
{
  // Group data into sets
  var tempList = [];
  for (var i=0; i<players.length; i++)
  {
    tempList.push( [players[i], scores[i], playerStatuses[i]] );
  }

  // Rearrange scores to find out who is leading the head
  tempList.sort(function(a,b) {
    return ((a[1] > b[1]) ? -1 : ((a[1] == b[1]) ? 0 : 1));
  });

  // Hide excess player list objects
  var numOfPlayerElements = $(".player").length;
  //console.log("numOfPlayerElements = " + numOfPlayerElements);
  for (var i=tempList.length; i<numOfPlayerElements; i++)
  {
    $("#player"+i)[0].className = "player disabled";
  }

  // Display a required element to represent a player
  for (var i=0; i<tempList.length; i++)
  {
    $("#player"+i)[0].className = "player";
    $("#playerText"+i).text("[ " + tempList[i][0] + " ] - Score ( " + tempList[i][1] + " )");
    $("#statusImg"+i).attr("src", tempList[i][2]===-1?"Images/PencilT.png":tempList[i][2]===0?"Images/noneT.png":"Images/correctT.png");
  }
}

// Timer Countdown
function formatTime(seconds)
{
  return seconds>9? seconds : "0" + seconds;
}

function startCountdown()
{
  if (timeRemaining > 0)
  {
    timeRemaining--;
    $("#countingTimer").text(formatTime(Math.floor(timeRemaining/60)) +
      " : " + formatTime(timeRemaining%60));
  }
  else
  {
    clearInterval(timeCounting);

    // Stop the player from drawing
    if (isDrawingClient)
    {
      setDrawingClient(false);
    }
  }
}

function startTimerCountdown()
{
  // Clear previous counting (if exist)
  clearInterval(timeCounting);

  // Put the initial time to UI
  $("#countingTimer").text(formatTime(Math.floor(timeRemaining/60)) +
    " : " + formatTime(timeRemaining%60));

  // Start the counting
  timeCounting = setInterval(startCountdown, 1000);
}

// Handy URL parameters retrieving function
// Code from: https://www.sitepoint.com/get-url-parameters-with-javascript/
function getAllUrlParams(url) {

  // get query string from url (optional) or window
  var queryString = url ? url.split('?')[1] : window.location.search.slice(1);

  // we'll store the parameters here
  var obj = {};

  // if query string exists
  if (queryString) {

    // stuff after # is not part of query string, so get rid of it
    queryString = queryString.split('#')[0];

    // split our query string into its component parts
    var arr = queryString.split('&');

    for (var i = 0; i < arr.length; i++) {
      // separate the keys and the values
      var a = arr[i].split('=');

      // set parameter name and value (use 'true' if empty)
      var paramName = a[0];
      var paramValue = typeof (a[1]) === 'undefined' ? true : a[1];

      // (optional) keep case consistent
      paramName = paramName.toLowerCase();
      if (typeof paramValue === 'string') paramValue = paramValue.toLowerCase();

      // if the paramName ends with square brackets, e.g. colors[] or colors[2]
      if (paramName.match(/\[(\d+)?\]$/)) {

        // create key if it doesn't exist
        var key = paramName.replace(/\[(\d+)?\]/, '');
        if (!obj[key]) obj[key] = [];

        // if it's an indexed array e.g. colors[2]
        if (paramName.match(/\[\d+\]$/)) {
          // get the index value and add the entry at the appropriate position
          var index = /\[(\d+)\]/.exec(paramName)[1];
          obj[key][index] = paramValue;
        } else {
          // otherwise add the value to the end of the array
          obj[key].push(paramValue);
        }
      } else {
        // we're dealing with a string
        if (!obj[paramName]) {
          // if it doesn't exist, create property
          obj[paramName] = paramValue;
        } else if (obj[paramName] && typeof obj[paramName] === 'string'){
          // if property does exist and it's a string, convert it to an array
          obj[paramName] = [obj[paramName]];
          obj[paramName].push(paramValue);
        } else {
          // otherwise add the property
          obj[paramName].push(paramValue);
        }
      }
    }
  }

  return obj;
}

function sendClearToServer()
{
  connection.send(JSON.stringify({ type: "clearCanvas" }));
}

function sendToolToServer(newTool)
{
  var message = {
    type: "newTool",
    data: newTool
  };
  connection.send(JSON.stringify(message));
  //console.log("sent " + JSON.stringify(message));
}

function sendColorToServer(newColor)
{
  var message = {
    type: "newColor",
    data: newColor
  };
  connection.send(JSON.stringify(message));
  //console.log("sent " + JSON.stringify(message));
}

function sendSizeToServer(newSize)
{
  var message = {
    type: "newSize",
    data: newSize
  };
  connection.send(JSON.stringify(message));
  //console.log("sent " + JSON.stringify(message));
}

function sendDrawingToServer(posX, posY)
{
  var message = {
    type: "syncDrawing",
    position: { x: posX, y: posY }
  };
  connection.send(JSON.stringify(message));
  //console.log("sent " + JSON.stringify(message));
}

function sendStopDrawingToServer()
{
  connection.send(JSON.stringify({ type:"stopDrawing" }));
}

// --- Main functions calling/Setups ---
$(document).ready(function()
{
  // Retrieve the player's name
  username = getAllUrlParams().username;
  //console.log("username = " + username);

  // Test the client's username to see if it is valid,
  // in case the user just input another username in the URL directly
  var regex = /[^a-z0-9]/gi;
  if (regex.test(username))
  {
    alert("Sorry, your username contains illegal characters.\n" +
      "Please login with another username.");

    // Redirect the player to the login page
    window.location.replace("index.html");
  }

  if (username.length < usernameLength_min || username.length > usernameLength_max)
  {
    alert("Sorry, the username has to be in " + usernameLength_min +
      " to " + usernameLength_max + " characters.\n" +
      "Please login with another username.");

    // Redirect the player to the login page
    window.location.replace("index.html");
  }

  // - Start connection with the server -
  connection = new WebSocket("ws://localhost:" + port);

  // When this client has successfully connected to the server
  connection.onopen = function()
  {
    // Send the server this client's username
    connection.send(JSON.stringify({ type:"init", data:username }));
  };

  connection.onerror = function(error)
  {
    // Something wrong happening
  };

  // Set up an event handler to process messages received from a server
  connection.onmessage = function(message)
  {
    //console.log("Received: " + message.utf8Data);

    // Try to parse JSON message
    try
    {
      var jsonMessage = JSON.parse(message.data);
    }
    catch (e)
    {
      console.log("Invalid JSON: " + message.data);
      return;
    }

    var messageType = jsonMessage.type;

    if (messageType === "serverFull")
    {
      alert("Oops!\n\nThe server is full.\nPlease join for another session.");

      window.location.replace("index.html");
    }
    else if (messageType === "gameStarted")
    {
      // Disable the inputs no matter the game has started or not
      // (Not started) The game is waiting enough players to start the game
      // (Started) The player can only join the game next round
      $("#guess").prop("disabled", true);
      $("#submitGuess").prop("disabled", true);

      if (jsonMessage.data === true)
      {
        gameStarted();
        $("#guess").val("The game has started! Please wait for the next round.");
      }
      else
      {
        $("#countingTimer").text("Commencing");
        $("#drawerText").text("- Draw & Guess -");

        waitPlayers(jsonMessage.data, false);
        $("#guess").val("Waiting for players...");
      }
    }
    else if (messageType === "startRound")
    {
      // A new round has started!
      clearLocalCanvas();

      // Activate the inputs for the guessers
      if (!isDrawingClient)
      {
        $("#guess").val("");
        $("#guess").prop("disabled", false);
        $("#submitGuess").prop("disabled", false);
      }
      else    // Disable the input fields for the drawer
      {
        $("#guess").val("Start drawing! You will also get a score if half of the players get what you mean.");
        $("#guess").prop("disabled", true);
        $("#submitGuess").prop("disabled", true);
      }
    }
    else if (messageType === "winningScore")
    {
      winningScore = jsonMessage.data;
    }
    else if (messageType === "playerList")
    {
      $("#playerListTitle").text("Current Players (" + jsonMessage.playerData.length + "/" + jsonMessage.maxPlayers + ")\nScore " + winningScore + " to win");
      updatePlayerList(jsonMessage.playerData, jsonMessage.scoreData, jsonMessage.statusData);
    }
    else if (messageType === "timeCountdown")
    {
      timeRemaining = jsonMessage.data;
      startTimerCountdown();
    }
    else if (messageType === "answer")
    {
      //$("#countingTimer").text("Time is up!");

      setDrawingClient(false);

      // Display the correct answer to the canvas
      displayAnswer(jsonMessage.data);
    }
    else if (messageType === "drawer")
    {
      setDrawingClient(jsonMessage.data);
      $("#drawerText").text((isDrawingClient ? "Draw the word: " + jsonMessage.answer : jsonMessage.drawerName + " is drawing..."));
    }
    else if (messageType === "clearCanvas")
    {
      clearLocalCanvas();
    }
    else if (messageType === "newTool")
    {
      setTool(jsonMessage.data);
    }
    else if (messageType === "newColor")
    {
      setColor(jsonMessage.data);
    }
    else if (messageType === "newSize")
    {
      setSize(jsonMessage.data);
    }
    else if (messageType === "syncDrawing")
    {
      var x = jsonMessage.position.x;
      var y = jsonMessage.position.y;

      //console.log("Received: " + x + " " + y);
      if ($(".tool.selected")[0].id === "fill")
      {
        flood_fill(x, y);
      }
      else
      {
        drawCanvas(x, y);
      }
    }
    else if (messageType === "stopDrawing")
    {
      stopDrawing();
    }
    else if (messageType === "drawerLeft")
    {
      // Tell this client the drawer has left (by canvas)
      notifyDrawerLeft();

      // Disable the inputs
      $("#guess").val("");
      $("#guess").prop("disabled", true);
      $("#submitGuess").prop("disabled", true);
    }
    else if (messageType === "allCorrect")
    {
      // Tell this client that all players got the answer (by canvas)
      allCorrect();
    }
    else if (messageType === "gamePaused")
    {
      setDrawingClient(false);

      clearInterval(timeCounting);
      $("#countingTimer").text("Game Paused");
      $("#drawerText").text("- Draw & Guess -");

      // Tell this client to wait (by canvas)
      waitPlayers(jsonMessage.data, true);
    }
    else if (messageType === "result")
    {
      // Server finds the answer correct after validation
      if (jsonMessage.data === true)
      {
        $("#guess").val("Well done! You got the answer.");

        // Scores will be updated by another message
      }
      else  // Wrong answer
      {
        // Allow the player to input another guess
        $("#guess").val("");
        $("#guess").prop("disabled", false);
        $("#submitGuess").prop("disabled", false);
      }
    }
    else if (messageType === "endGame")
    {
      clearInterval(timeCounting);

      setDrawingClient(false);

      $("#countingTimer").text("Game End");
      $("#drawerText").text("- Draw & Guess -");

      // Display the winner(s) on the canvas
      displayWinners(jsonMessage.data);

      // Shut down the inputs
      $("#guess").val("The game has ended.");
      $("#guess").prop("disabled", true);
      $("#submitGuess").prop("disabled", true);
    }
    else
    {
      console.log("Undefined jsonMessage received from server: " + jsonMessage);
    }
  };

  // Add listener: when an exception occurs
	connection.onerror = function(event)
	{
    alert("Lost connection with the server.");

    // Redirect the player to the login page
    window.location.replace("index.html");
	};

  // Allow the player to focus on input field when he presses 'Tab' (shortcut)
  $(document).attr("body").addEventListener("keydown", function(event)
  {
    if(event.keyCode === 9)
    {
      //console.log("Tab pressed");
      event.preventDefault();
      $("#guess")[0].focus();
    }
  });

  // When the submit button is pressed
  function submitAnswer()
  {
    // Send the answer to the server for validation
    connection.send(JSON.stringify({ type:"guess", data:$("#guess").val().toLowerCase() }));

    // Temporarily disable the inputs when the validation process undergoes
    $("#guess").prop("disabled", true);
    $("#submitGuess").prop("disabled", true);
  }

  // Allow the player to submit his answer when he clicks the submit button
  $("#submitGuess").click(submitAnswer);
  // Also allow him to submit when 'Enter' is pressed
  $("#guess")[0].addEventListener("keyup", function(event)
  {
    event.preventDefault();
    if (event.keyCode === 13)
    {
      submitAnswer();
    }
  });
});
