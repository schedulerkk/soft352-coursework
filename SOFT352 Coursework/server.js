
/* Configuration */
var maxPlayers = 10;						// The maxumum amount of player allowed
var playersToStartGame = 3;			// Minimum players required to start the game
var scoreToEndGame = 5;					// The final score a player reaches to end the game
var timePerRound = 90;					// Time interval per round (in seconds)
var timeForDisplayAnswer = 5;		// Time interval for displaying the answer to clients (in seconds)
var timeAllCorrectToStart = 5;	// Time interval to start the round after all player got the answer (in seconds)
var timeDrawerLeftToEnd = 3;		// Time interval to end the round after the drawer has left the game (in seconds)

/* Internal variables */
var port = 9000;					// The server's port number
var connectionArray = [];	// An array holding active client connections
var usernameArray = [];		// An array holding active clients' usernames
var scoreArray = [];			// An array holding active clients' scores
var correctGuessers = [];	// An array holding the number of guessers who got the answer
var justJoined = [];			// An array holding newly joined clients (refreshes every round) (Skip sending the sync drawing messages)
var winners = [];					// An array storing the winners of this game

var gameStarted = false;	// Indicates whether the game has started
var gamePaused = false;		// Indicates whether the game has paused (not enough players)
var roundEnded = false;		// Indicates whether the round has ended
var gameEnded = false;		// Indicates whether the game has ended
var answer;								// The correct answer
var answerId;							// The correct answer's id (used for checking whether it is the same as the previous answer)
var timeRemaining;				// The remaining time in seconds
var timeCounting;					// Stores the id for clearInterval
var displayanswerTimeout;	// Stores the id for clearTimeout
var endRoundTimeout;			// Stores the id for clearTimeout
var drawer;								// The current drawer's index
	// Players joined after the game has ended can also read the winners
var drawerBonus;					// Indicates whether the drawer has gain his bonus after half of the players have got the answer

/* Import the relevant module */
var WebSocketServer = require("websocket").server;
var http = require("http");
var mysql = require("mysql");

/* Internal functions */

// Broadcast an updated player list (including score)
function broadcastPlayerList()
{
	// Store the current status of each player
	// [-1=drawer, 0=guessing, 1=correct]
	var statusList = [];
	for (var i=0; i<correctGuessers.length; i++)
	{
		statusList.push(i===drawer?-1:correctGuessers[i]?1:0);
	}

	var json = JSON.stringify({ type:"playerList", maxPlayers:maxPlayers, playerData:usernameArray, scoreData: scoreArray, statusData: statusList });
	for (var i=0; i<connectionArray.length; i++)
	{
		connectionArray[i].sendUTF(json);
	}
}

// Compare the objects instead of using a stored index at the beginning
// in case of dynamically changed index.
// e.g. a client at the front of the array left
function findClientIndex(connection)
{
	for(var i=0; i<connectionArray.length; i++)
	{
		if (connection === connectionArray[i])
		{
			return i;
		}
	}
}

// Standard format for logging messages to the server console
function logConsole(message)
{
	console.log("[" + new Date () + "] - " + message);
}

function retrieveAnswerFromDatabase()
{
	// Create a Promise object to stall the further operations
	return new Promise(function(resolve, reject)
	{
		mysqlConn.query("SELECT id FROM answer", (error1, result) =>
		{
			if (error1)
				reject(new Error("Error occured when getting the answer count"));

			// Store the ids into an array and randomize it, in case
			// the answers' id in database does not start with 1
			// or some ids are missing in the middle
			var idArray = [];

			for (var i=0; i<result.length; i++)
			{
				idArray.push(result[i].id);
				//console.log("Pushed " + result[i].id + " into idArray");
			}

			// Generate a new answer by specifying it's id
			// Make sure the newly generated answer is not the same as the original one
			var id;
			do {
				id = idArray[Math.floor(Math.random() * idArray.length)];
			} while (id === answerId);

			answerId = id;

			mysqlConn.query("SELECT data FROM answer WHERE id = " + id, (error2, retrievedAnswer) =>
			{
				if (error2)
					reject(new Error("Error occured when getting the answer"));

				//console.log("Answer from the database: " + retrievedAnswer[0].data + " (id: " + id + ")");

				resolve(retrievedAnswer[0].data);
			});
		})
	})
}

function generateAnswer()
{
	retrieveAnswerFromDatabase().then(function(retrievedAnswer)
	{
		// Do the remaining procedures for starting the round
		answer = retrievedAnswer;
		logConsole("New generated answer: " + answer);

		determineDrawer();
		setTimer(timePerRound);

		clearTimeout(endRoundTimeout);
		endRoundTimeout = setTimeout(endRound, timePerRound * 1000);

		// Reset all guessing status
		for (var i=0; i<correctGuessers.length; i++)
		{
			correctGuessers[i] = false;
		}

		// Broadcast the start round message to all the clients for enabling/disabling fields
		for (var i=0; i<connectionArray.length; i++)
		{
			connectionArray[i].sendUTF(JSON.stringify({ type:"startRound" }));
		}

		// Update who is the drawer to the client's playerList
		broadcastPlayerList();
	})
	.catch(function(error)
	{
		console.log("Promise error occured: " + error);
	});
}

function determineDrawer()
{
	var drawerIndex = drawer;

	// Make sure the newly picked drawer is not the same guy
	while (drawer === drawerIndex)
	{
		drawer = Math.floor(Math.random() * connectionArray.length);
	}

	var drawerName = usernameArray[drawer];
	logConsole("Player (" + drawerName + ") is the drawer");

	// Broadcast the drawer to all the players
	for (var i=0; i<connectionArray.length; i++)
	{
		connectionArray[i].sendUTF(JSON.stringify({ type:"drawer", data:(i===drawer), drawerName:drawerName, answer:answer }));
	}
}

function startCountdown()
{
  if (timeRemaining > 0)
  {
    timeRemaining--;
  }
  else
  {
		clearInterval(timeCounting);
  }
}

function setTimer(time)
{
	// Clear previous counting (if exist)
	clearInterval(timeCounting);

	timeRemaining = time;

	// Broadcast the timer countdown to all the players
	var json = JSON.stringify({ type:"timeCountdown", data:timeRemaining });
	for (var i=0; i<connectionArray.length; i++)
	{
		connectionArray[i].sendUTF(json);
	}

  // Start the counting
  timeCounting = setInterval(startCountdown, 1000);
}

function checkEnoughPlayers()
{
	// Skip checking if the game has already been paused
	if (gamePaused)
	{
		return false;
	}

	// Check if any one has win the game before pausing the game
	if (checkWinner())
	{
		return false;		// Stop executing other lines
	}

	if (connectionArray.length < playersToStartGame)
	{
		// There are not enough players to continue the game, wait for more!
		gamePaused = true;

		clearInterval(timeCounting);

		// Notify all the client the game has paused
		var json = JSON.stringify({ type:"gamePaused", data:playersToStartGame });
		for (var i=0; i<connectionArray.length; i++)
		{
			connectionArray[i].sendUTF(json);
		}

		logConsole("****************************************************");
		logConsole("Game paused: not enough players");

		return false;
	}

	return true;
}

function checkHalfCorrect()
{
	// Skip checking if the game round has ended
	if (roundEnded)
	{
		return;
	}

	// Skip checking if the reward has already given
	if (drawerBonus)
	{
		return;
	}

	var newComers = 0;
	for (var i=0; i<justJoined.length; i++)
	{
		if (justJoined[i])
		{
			newComers++;
		}
	}

	var threshold = Math.ceil((correctGuessers.length-newComers-1)/2);	// Exclude drawer too

	for (var i=0; i<correctGuessers.length; i++)
	{
		// Skip checking the drawer
		if (i === drawer)
		{
			continue;
		}

		// Skip checking the newly joined clients (they can't even make a guess!)
		if (justJoined[i])
		{
			continue;
		}

		if (correctGuessers[i] === true)
		{
			threshold--;
		}
	}

	if (threshold <= 0)
	{
		logConsole("Half of the guessers got the answer.");

		drawerBonus = true;

		// Add a score to the drawer
		scoreArray[drawer]++;

		// The score list will be updated together with the guesser's score
	}
}

function checkAllCorrect()
{
	// Skip checking if the game round has ended
	if (roundEnded)
	{
		return;
	}

	for (var i=0; i<correctGuessers.length; i++)
	{
		// Skip checking the drawer
		if (i === drawer)
		{
			continue;
		}

		// Skip checking the newly joined clients (they can't even make a guess!)
		if (justJoined[i])
		{
			continue;
		}

		if (correctGuessers[i] === false)
		{
			return;
		}
	}

	logConsole("All the guessers got the answer.");

	roundEnded = true;

	// Add a score to the drawer
	scoreArray[drawer]++;

	// Update the current score list to all players
	broadcastPlayerList();

	var json = JSON.stringify({ type:"allCorrect" });
	for (var i=0; i<connectionArray.length; i++)
	{
		connectionArray[i].sendUTF(json);
	}

	setTimer(timeAllCorrectToStart);

	// Answer is not shown to clients if all got the answer
	clearTimeout(endRoundTimeout);
	endRoundTimeout = setTimeout(startRound, timeAllCorrectToStart * 1000);
}

function checkWinner()
{
	// Check if any player has won the game already
	// There maybe multiple players getting the same score in the last round
	winners = [];
	for (var i=0; i<scoreArray.length; i++)
	{
		if (scoreArray[i] >= scoreToEndGame)
		{
			winners.push(usernameArray[i]);
		}
	}

	// End the game if there are winners
	if (winners.length > 0)
	{
		endGame();
		return true;
	}

	return false;
}

function startRound()
{
	// Check if there is enough players before we continue the game
	if (!checkEnoughPlayers())
	{
		return;
	}

	logConsole("****************************************************");
	logConsole("A new round has started.");

	roundEnded = false;
	drawerBonus = false;

	for (var i=0; i<justJoined.length; i++)
	{
		justJoined[i] = false;
	}

	generateAnswer();
}

function endRound()
{
	roundEnded = true;

	// Display the answer on the client-side
	var json = JSON.stringify({ type:"answer", data:answer });
	for (var i=0; i<connectionArray.length; i++)
	{
		connectionArray[i].sendUTF(json);
	}

	setTimer(timeForDisplayAnswer);
	clearTimeout(displayanswerTimeout);
	displayanswerTimeout = setTimeout(startRound, timeForDisplayAnswer * 1000);
}

function endGame()
{
	roundEnded = true;
	gameEnded = true;

	// Clear all timing events
	clearInterval(timeCounting);
	clearTimeout(endRoundTimeout);

	logConsole("The game has ended. Winner" + (winners.length>1?"s":"") + ": " + winners);
	logConsole("****************************************************");

	// Broadcast the winning player(s) to all the clients
	for (var i=0; i<connectionArray.length; i++)
	{
		connectionArray[i].sendUTF(JSON.stringify({ type:"endGame", data:winners }));
	}
}

// MySQL connection
var mysqlConn = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
	database: "soft352"
});

mysqlConn.connect(function(error)
{
  if (error)
	{
		console.error("Failed to connect to MySQL database.");
		process.exit(1);
		//throw error;
	}

  logConsole("Successfully connected to MySQL database.");
});

// Websocket - instantiate a socket object
var server = http.createServer (function (request, response)
{
	logConsole("Received request");
});

server.listen(port, function()
{
	logConsole("Server listening on port " + port);
});

var socket = new WebSocketServer({
	httpServer: server
});

// When a player tries to connect to the WebSocket server
socket.on("request", function(request)
{
	logConsole("Connection request from origin" + request.origin);

	var connection = request.accept(null, request.origin);

	// Check if the current amount of connected clients reaches the maximum limit
	if (connectionArray.length >= maxPlayers)
	{
		logConsole("Refused connection from client due to server full.");

		// Refuse the connection request
		connection.sendUTF(JSON.stringify({ type:"serverFull" }));
		connection.close();
		return;
	}

	// Add the connection to an array of connections
	connectionArray.push(connection);

	// Add listener: when a message is received from a client
	connection.on("message", function(message)
	{
		if (message.type === "utf8")
		{
			//console.log(message);

			// Try to parse JSON message
	    try
	    {
	      var jsonMessage = JSON.parse(message.utf8Data);
	    }
	    catch (e)
	    {
	      console.log("Invalid JSON: " + message.data);
				return;
	    }

			var messageType = jsonMessage.type;

			if (messageType === "init")
			{
				var username = jsonMessage.data;
				usernameArray.push(username);
				scoreArray.push(0);
				correctGuessers.push(false);
				justJoined.push(true);
				logConsole("Player (" + username + ") has joined the game.");

				// Assuming the winning score will not change during the game
				// Send once only when the player has joined the game
				connection.sendUTF(JSON.stringify({ type:"winningScore", data:scoreToEndGame }));

				// Update all clients' player list
				broadcastPlayerList();

				// Tell the player whether the game has already started
				connection.sendUTF(JSON.stringify({ type:"gameStarted", data:(gameStarted? true : playersToStartGame) }));

				// Send the newly-joined client some info according to current game status
				if (gameEnded)
				{
					// Unfortunately, the game has already ended =[
					connection.sendUTF(JSON.stringify({ type:"endGame", data:winners }));
				}
				else if (connectionArray.length >= playersToStartGame &&
					(!gameStarted || gameStarted && gamePaused))
				{
					// Start/Continue the game if we've got enough players
					gameStarted = true;
					gamePaused = false;
					roundEnded = false;
					gameEnded = false;
					startRound();
				}
				else if (gameStarted)
				{
					// The game is in progress
					connection.sendUTF(JSON.stringify({ type:"timeCountdown", data:timeRemaining }));
					connection.sendUTF(JSON.stringify({ type:"drawer", data:false, drawerName:usernameArray[drawer], answer:"" }));
				}
			}
			else if (messageType === "guess")
			{
				// Verify the player's answer
				if (jsonMessage.data === answer)
				{
					var playerIndex = findClientIndex(connection);

					logConsole("Player (" + usernameArray[playerIndex] + ") has got the answer.");

					scoreArray[playerIndex]++;
					correctGuessers[playerIndex] = true;

					// Reward a score to the drawer if half of the guessers got the answer
					checkHalfCorrect();

					// Tell the player he nailed it!
					connection.sendUTF(JSON.stringify({ type:"result", data:true }));

					// Update the current score list to all players
					broadcastPlayerList();

					// End the round if all the guessers have got the answer
					checkAllCorrect();
				}
				else
				{
					// Prompt the player to make another guess
					connection.sendUTF(JSON.stringify({ type:"result", data:false }));
				}
			}
			else if (messageType === "clearCanvas" || messageType === "syncDrawing" || messageType === "stopDrawing" ||
				messageType === "newTool" || messageType === "newColor" || messageType === "newSize")
			{
				// The drawer client wants to broadcast the message to other guesser clients
				// Just send out the original message to other clients

				//console.log(jsonMessage);

				// Broadcast the received message to the other clients
				for (var i=0; i<connectionArray.length; i++)
				{
					// Skip sending the sync messages to the drawer
					if (connectionArray[i] === connection)
					{
						continue;
					}

					// Skip sending the sync messages to the newly joined clients
					if (justJoined[i])
					{
						continue;
					}

					connectionArray[i].sendUTF(JSON.stringify(jsonMessage));
					//console.log("sending out: " + JSON.stringify(jsonMessage));
				}
			}
			else
	    {
	      logConsole("Undefined jsonMessage received from client: " + jsonMessage);
	    }
		}
	});

	// Add listener: when client closes connection
	connection.on("close", function(reasonCode, description)
	{
		var clientIndex = findClientIndex(connection);
		logConsole("Player (" + usernameArray[clientIndex] + ") disconnected.");

		// Remove the closed connection from active connection array
		connectionArray.splice(clientIndex, 1);
		// Also remove the player data
		// The clientIndex should be the same as connectionArray
		usernameArray.splice(clientIndex, 1);
		scoreArray.splice(clientIndex, 1);
		correctGuessers.splice(clientIndex, 1);
		justJoined.splice(clientIndex, 1);

		broadcastPlayerList();

		// Make sure the game round is in progress
		if (!gamePaused && !roundEnded && !gameEnded)
		{
			// Check if the left client is the drawer
			// clientIndex will not be changed even if the arrays are modified
			if (clientIndex === drawer)
			{
				setTimer(timeDrawerLeftToEnd);

				clearTimeout(endRoundTimeout);
				endRoundTimeout = setTimeout(endRound, timeDrawerLeftToEnd * 1000);

				// Notify all the client the drawer has left the game
				var json = JSON.stringify({ type:"drawerLeft" });
				for (var i=0; i<connectionArray.length; i++)
				{
					connectionArray[i].sendUTF(json);
				}
			}
			else
			{
				// The left client is a guesser
				if (!checkEnoughPlayers())
				{
					return;
				}

				// End the round if the other guessers have all got the answer
				checkAllCorrect();
			}
		}
	});
});
