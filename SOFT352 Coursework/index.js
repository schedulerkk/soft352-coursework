
/* Configuration */
var usernameLength_min = 3;   // The minimum length of a username
var usernameLength_max = 10;  // The maximum length of a username

function validateName()
{
  var username = document.forms["nameForm"]["username"].value;

  // Check if the player's username fulfill the system's naming requirements
  if (username.length < usernameLength_min || username.length > usernameLength_max)
  {
    alert("Sorry, the username has to be in " + usernameLength_min +
      " to " + usernameLength_max + " characters.");
    return false;
  }

  // Save username to localStorage
  localStorage.username = username;

  // Redirect the player to join the game!
  //window.location.replace("game.html?username=" + username);
  //$("#nameForm").submit();
}

function checkUsernameValid(input)
{
  // Regular expression
  var regex = /[^a-z0-9]/gi;
  input.value = input.value.replace(regex, "");
}

function loadUsername()
{
  var username = localStorage.username;
  if (username !== undefined)
  {
    document.forms["nameForm"]["username"].value = username;
  }
}

// --- Main functions calling/Setups ---
$(document).ready(function()
{
  // Allow the player to focus on input field when he presses 'Tab' (shortcut)
  $(document).attr("body").addEventListener("keydown", function(event)
  {
    if(event.keyCode === 9)
    {
      //console.log("Tab pressed");
      event.preventDefault();
      $("input:text").focus();
    }
  });

  loadUsername();
});
