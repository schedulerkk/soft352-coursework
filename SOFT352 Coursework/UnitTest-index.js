
// Tests for structure/html
QUnit.test("#Unit test 1 testing page title", function(assert)
{
  assert.ok($(document).attr("title").indexOf("Draw & Guess")!=-1,"Checking the page title");
});

QUnit.test("#Unit test 2 testing content head", function(assert)
{
  assert.equal($(".contentHead")[0].tagName, "DIV", "Checking that a \'contentHead\' div element has been added");
});

QUnit.test("#Unit test 3 testing content", function(assert)
{
  assert.equal($(".content")[0].tagName, "DIV", "Checking that a \'content\' div element has been added");
  assert.equal($(".content .container")[0].tagName, "DIV", "Checking that a \'container\' div element has been added as a child of \'content\'");
  assert.equal($(".content .container .title")[0].tagName, "DIV", "Checking that a \'title\' div element has been added as a child of \'container\'");
  assert.equal($(".content .container .title p")[0].tagName, "P", "Checking that a p element has been added as a child of \'title\'");
  assert.equal($(".content .container input")[0].type, "text", "Checking that an input text element has been added as a child of \'container\'");
  assert.equal($(".content .container button")[0].type, "submit", "Checking that a submit button element has been added as a child of \'container\'");
});


// Tests for style/css
QUnit.test("#Unit test 4 testing body format", function(assert)
{
  assert.equal($("body").css("height"),"680px","Checking that body height is 680px");
  //console.log(parseInt(window.getComputedStyle(document.body).getPropertyValue('margin').replace('px',''), 10));
  assert.equal($("body").css("margin"), "15px", "Checking that the body margin is 15px");
  assert.equal($("body").css("padding"), "0px", "Checking that the body padding is 0px");
});

QUnit.test("#Unit test 5 testing content style", function(assert)
{
  assert.equal($(".content").css("text-align"), "center", "Checking that the content has a \'center\' text-align property");
});

QUnit.test("#Unit test 6 testing container style", function(assert)
{
  assert.equal($(".content .container").css("text-align"), "center", "Checking that the container has a \'center\' text-align property");
  assert.equal($(".content .container").css("border-radius"), "20px", "Checking that the container border-radius is 20px");
});

QUnit.test("#Unit test 7 testing title text style", function(assert)
{
  assert.equal($(".content .title p").css("font-size"), "50px", "Checking that the content title font-size is 50px");
  // Bold = 700
  assert.equal($(".content .title p").css("font-weight"), "700", "Checking that the content title has a \'bold\' font-weight property");
});

QUnit.test("#Unit test 8 testing username's style", function(assert)
{
  assert.equal($(".content .container input").css("font-size"), "30px", "Checking that the username font-size is 30px");
  assert.equal($(".content .container input").css("text-align"), "center", "Checking that the username's text is centered");
  assert.equal($(".content .container input").css("border-radius"), "10px", "Checking that the username border-radius is 10px");
});

QUnit.test("#Unit test 9 testing submit button's style", function(assert)
{
  assert.equal($(".content .container button").css("font-size"), "30px", "Checking that the submit button text's font-size is 30px");
  assert.equal($(".content .container button").css("border-radius"), "10px", "Checking that the submit button's border-radius is 10px");
  // #333 = rgb(51, 51, 51)
  assert.equal($(".content .container button").css("background-color"), "rgb(51, 51, 51)", "Checking that the submit button's background-color is rgb(51, 51, 51)");
  assert.equal($(".content .container button").css("color"), "rgb(255, 255, 255)", "Checking that the submit button's background-color is rgb(255, 255, 255)");
});
